Structurile de date principale pentru memorarea locurilor si caracteristicilor sale
sunt HashMap pentru cautare rapida (cerinta 1) si TreeMap pentru sortare dupa pret si
alegere locului in ordinea crescatoare a pretului (cerintele 2,3). Am ales acest tradeoff
de consum mai mare de memorie si mai mic de timp pentru a ma folosi de plusurile ambelor 
structuri. 
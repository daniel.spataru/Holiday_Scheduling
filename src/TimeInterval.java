import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by Dani on 15-Mar-18.
 */
public class TimeInterval {

	private SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
	private Date start, end;

	TimeInterval(String startDate, String endDate) {
		try {
			start = sdf.parse(startDate);
			end = sdf.parse(endDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	/*verifica intervalul de timp daca este de cel putin 10 zile*/
	boolean atLeast10days() {
		return((end.getTime() - start.getTime()) >= TimeUnit.DAYS.toMillis(10));
	}

	/*verifica disponibilitatea locului de vacanta in intervalul declarat*/
	boolean containsInterval(TimeInterval interval) {
		return start.compareTo(interval.start) <= 0 && end.compareTo(interval.end) >= 0;
	}

	public String toString() {
		return "Period " + start.toString() + " - " + end.toString();
	}

}

/**
 * Created by Dani on 15-Mar-18.
 */
public class Address {

	private String village = "";
	private String locality = "";
	private String town;
	private String county;
	private String country;

	Address(String town, String county, String country) {
		this.town = town;
		this.county = county;
		this.country = country;
	}

	Address(String locality, String town, String county, String country) {
		this(town, county, country);
		this.locality = locality;
	}

	Address(String village, String locality, String town, String county, String country) {
		this(locality, town, county, country);
		this.village = village;
	}

	public String getTown() {
		return town;
	}

	public String getCounty() {
		return county;
	}

	public String getCountry() {
		return country;
	}

	public String getLocality() {
		return locality;
	}

	public String getVillage() {
		return village;
	}

	public String toString() {

		StringBuilder stringBuilder = new StringBuilder("Address is: ");

		if (!village.equals(""))
			stringBuilder.append(village + ", ");
		if (!locality.equals(""))
			stringBuilder.append(locality + ", ");

		stringBuilder.append(town + ", " + county + ", " + country);
		return stringBuilder.toString();
	}

}

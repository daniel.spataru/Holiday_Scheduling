import java.util.*;

/**
 * Created by Dani on 15-Mar-18.
 */
public class Holiday {

	private static final String FILENAME = "input_file.txt";
	private static final int topLocations = 5;

	private static void getLocations(MyScanner sc, HashMap<String, Location> hashMap, TreeMap<Double, Location> treeMap) {

		StringTokenizer st;
		Location location;
		String name;
		String startDate, endDate;
		Double price;
		Address address;
		TimeInterval timeInterval;
		/*Collection of activities for fast search*/
		HashSet<String> activities = new HashSet<>();
		/*Informations about address of location*/
		ArrayList<String> info_addr = new ArrayList<>();

		while ((name = sc.nextLine()) != null) {

			st = getStringTokenizer(sc);
			while (st.hasMoreElements())        /*read address info*/
				info_addr.add(st.nextToken());
			address = getAddress(info_addr);
			info_addr.clear();

			price = sc.nextDouble();            /*read price info*/

			st = getStringTokenizer(sc);
			while (st.hasMoreElements())        /*read activities*/
				activities.add(st.nextToken());

			st = getStringTokenizer(sc);        /*read period of holiday*/
			startDate = st.nextToken();
			endDate = st.nextToken();
			timeInterval = new TimeInterval(startDate, endDate);

			location = new Location(name, address, price, activities, timeInterval);
			hashMap.put(name, location);    /*useful for fast search by name*/
			treeMap.put(price, location);   /*useful for getting cheapest holidays*/
			activities.clear();
		}
	}

	/*get Address instance by informations of address*/
	private static Address getAddress(ArrayList<String> info_addr) {
		Address address;
		switch (info_addr.size()) {
			case 5:
				address = new Address(info_addr.get(0), info_addr.get(1), info_addr.get(2),
						info_addr.get(3), info_addr.get(4));
				break;
			case 4:
				address = new Address(info_addr.get(0), info_addr.get(1), info_addr.get(2),
						info_addr.get(3));
				break;
			default:
				address = new Address(info_addr.get(0), info_addr.get(1), info_addr.get(2));
		}
		return address;
	}

	private static StringTokenizer getStringTokenizer(MyScanner sc) {
		String line;
		StringTokenizer st;
		line = sc.nextLine();
		st = new StringTokenizer(line, ",-");
		return st;
	}


	public static void main(String[] args) {

		MyScanner sc = new MyScanner(FILENAME);
		HashMap<String, Location> hashMap = new HashMap<>();
		TreeMap<Double, Location> treeMap = new TreeMap<>();
		Holiday.getLocations(sc, hashMap, treeMap);             //load locations from file
		/*Requirement 1*/
		System.out.println("Available locations:");
		System.out.println(hashMap.keySet());
		System.out.println("Insert desired location or exit for stop");
		sc = new MyScanner(System.in);
		String command = sc.next();
		while (!command.equals("exit")) {                //read commands from keyboard
			if (hashMap.containsKey(command))
				System.out.println(hashMap.get(command));
			else
				System.out.println("Non-existent location");
			command = sc.next();
		}

		System.out.println("Insert country/county/town/locality/village");
		command = sc.next();
		System.out.println("Insert period A-B");
		TimeInterval timeInterval = new TimeInterval(sc.next(), sc.next());
		System.out.println("Cheapest " + topLocations + " locations from " + command + " are: ");

		/*Requirement 2*/
		Location loc;
		int count = 0;
		/*Searching for first 5 locations from some address in period A-B*/
		for (Map.Entry<Double, Location> entry : treeMap.entrySet()) {
			loc = entry.getValue();
			if (loc.checkAddress(command) && loc.getTimeInterval().containsInterval(timeInterval)) {
				System.out.println(loc.toString());
				count++;
				if (count == topLocations)
					break;
			}
		}

		/*Requirement 3*/
		Map.Entry<Double, Location> map = treeMap.firstEntry();
		Location cheapestLoc = map.getValue();                      //First entry has lowest price
		if(cheapestLoc.getTimeInterval().atLeast10days())
			System.out.println("Cheapest 10 days holiday is : " +
				cheapestLoc.getName() + " " + cheapestLoc.getAddress().toString() + " at price " +
				map.getKey());

		System.out.println("Insert activity: ");
		command = sc.next();

		/*Searching for first entry that contains that activity*/
		for (Map.Entry<Double, Location> entry : treeMap.entrySet()) {
			loc = entry.getValue();
			if (loc.checkActivity(command) && loc.getTimeInterval().atLeast10days()) {
				System.out.println("Cheapest " + command + " activity is at: " +
						loc.getName() + " " + loc.getAddress().toString() + " at price " + entry.getKey());
				break;
			}
		}
	}

}

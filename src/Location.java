import java.util.HashSet;

/**
 * Created by Dani on 15-Mar-18.
 */
public class Location {

	private String name;
	private Address address;
	private Double price;
	private HashSet<String> activities;
	private TimeInterval interval;

	Location(String name, Address address, Double price, HashSet<String> activities, TimeInterval interval) {
		this.name = name;
		this.address = address;
		this.price = price;
		this.activities = new HashSet<>(activities);
		this.interval = interval;
	}

	/*identifica locul de vacanta pe baza unui camp al adresei*/
	public boolean checkAddress(String addr) {
		return address.getVillage().equals(addr) || address.getLocality().equals(addr)
				|| address.getCounty().equals(addr) || address.getTown().equals(addr)
				|| address.getCountry().equals(addr);
	}

	/*verifica daca locul dispune de activitatea respectiva*/
	public boolean checkActivity(String activity) {
		return activities.contains(activity);
	}

	public String getName() {
		return name;
	}

	public Address getAddress() {
		return address;
	}

	public Double getPrice() {
		return price;
	}

	public HashSet<String> getActivities() {
		return activities;
	}

	public TimeInterval getTimeInterval() {
		return interval;
	}

	public String toString() {
		return name + " " + address.toString() + " " + price + " " + activities + " " + interval.toString();
	}

}

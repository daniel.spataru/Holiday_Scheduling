import java.io.*;
import java.util.StringTokenizer;

/**
 * Created by Dani on 15-Mar-18.
 */
public class MyScanner {

	private BufferedReader br;
	private StringTokenizer st;

	/*citire input din fisier*/
	public MyScanner(String input) {
		try {
			br = new BufferedReader(new InputStreamReader(new FileInputStream(input)));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	/*citire input de la tastatura*/
	public MyScanner(InputStream in) {
		br = new BufferedReader(new InputStreamReader(in));
	}

	/*parseaza si citeste urmatorul cuvant*/
	String next() {
		while (st == null || !st.hasMoreElements()) {
			try {
				String line = br.readLine();
				if (line != null)
					st = new StringTokenizer(line, ",-");
				else
					return null;
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return st.nextToken();
	}

	/*citeste o valoare double de la intrare*/
	double nextDouble() {
		return Double.parseDouble(next());
	}

	/*citeste urmatoarea linie*/
	String nextLine() {
		String str = "";
		try {
			str = br.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return str;
	}
}
